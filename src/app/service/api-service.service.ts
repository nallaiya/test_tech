import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class ApiServiceService {
  serverUrl = environment.serverUrl;
  httpOptions:any;
  constructor(private http:HttpClient) { 
    this.httpOptions = {
      headers: {
        "Content-Type": "application/json"
      },
      params: new HttpParams()
    };
  }
  login(req:any): Observable<any> {
  
    return this.http.post(this.serverUrl + `record-holder/record-holer-login`, req, this.httpOptions)
  }
  otp(req:any): Observable<any> {
  
    return this.http.post(this.serverUrl + `record-holder/otp-verify`, req, this.httpOptions)
  }
  getProduct(req:any): Observable<any> {
  
    return this.http.post(this.serverUrl + `api/list-product`, req, this.httpOptions)
  }
  getInsituction(req:any): Observable<any> {
  
    return this.http.post(this.serverUrl + `api/list-institution`, req, this.httpOptions)
  }
 listDocument(req:any): Observable<any> {
  
    return this.http.post(this.serverUrl + `record-holder/document-list`, req, this.httpOptions)
  }
  addDocument(req:any): Observable<any> {
  
    return this.http.post(this.serverUrl + `record-holder/add-document`, req, this.httpOptions)
  }
  editDocument(req:any): Observable<any> {
  
    return this.http.post(this.serverUrl + ` record-holder/edit-document`, req, this.httpOptions)
  }
  
}
