import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StoreServiceService {

  private dataSource = new BehaviorSubject<any>('');
  data = this.dataSource.asObservable();
    constructor() { 
      
    }
  
    updatedDataSelection(datas: any){
      this.dataSource.next(datas);
     
    }
}
