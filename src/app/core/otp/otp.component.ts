import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiServiceService } from 'src/app/service/api-service.service';
import {Md5} from 'ts-md5/dist/md5';
@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.scss']
})
export class OtpComponent implements OnInit {
  otp: any
  user_Id: any
  constructor(private router: Router, private apiService: ApiServiceService) { }

  ngOnInit(): void {
    this.user_Id=localStorage.getItem('user_Id')
  }
  Verifiy() {
    const md5 = new Md5();
    let salt = 'FutureWill@TT)(*&^!@2020';
    let auth_token = salt + this.user_Id + this.otp;
    let  md5_token:any = md5.appendStr(auth_token).end(); 
    let req = {
      "user_id": this.user_Id,
      "otp": this.otp,
      "auth_token": md5_token
    }
    this.apiService.otp(req).subscribe(res => {

    })
    this.router.navigate(['/home/dashboard'])
  }
}
