import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiServiceService } from 'src/app/service/api-service.service';
import {Md5} from 'ts-md5/dist/md5';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  mobile_number:any;
  constructor(private router:Router,private apiService:ApiServiceService) { }

  ngOnInit(): void {
  }

  siginIn(){
    const md5 = new Md5();

    if(this.mobile_number !== undefined){
      let req:any;
      let salt = 'FutureWill@TT)(*&^!@2020';
      let auth_token = salt + this.mobile_number;
      let  md5_token:any = md5.appendStr(auth_token).end(); 
      localStorage.setItem('key', md5_token);
   
       req=
         {
          "mobile_number":this.mobile_number,
          "auth_token":md5_token
          }
          console.log(req);
          this.apiService.login(req).subscribe(res=>{
            console.log(res);
            localStorage.setItem('user_Id',res.user_id)
          })
      this.router.navigate(['/otp'])
    }else{
      alert("Enter Number")
    }
   
  }
}
