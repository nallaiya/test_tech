import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { OtpComponent } from './otp/otp.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
const routes: Routes = [
 {path : 'login',component:LoginComponent},
 {path:'otp',component:OtpComponent},
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },

]

@NgModule({
  declarations: [ LoginComponent,
    OtpComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forChild(routes)

  ]
})
export class AuthModule { }
