import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { AddRecordComponent } from './add-record/add-record.component';
import { UpdateRecordComponent } from './update-record/update-record.component';
import { RouterModule, Routes } from '@angular/router';
import { PrivateComponent } from './private/private.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  {path : '',component:PrivateComponent,
  children: [
    {
      path: 'dashboard',
      component:HomeComponent
    },
    {
      path: 'record/:id',
      component:AddRecordComponent
    }
  ]},
  
 ]

@NgModule({
  declarations: [
    HomeComponent,
    AddRecordComponent,
    UpdateRecordComponent,
    PrivateComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ],exports:[RouterModule]
})
export class DashboardModule { }
