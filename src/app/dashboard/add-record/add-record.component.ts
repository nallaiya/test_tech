import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiServiceService } from 'src/app/service/api-service.service';
import { StoreServiceService } from 'src/app/service/store-service.service';

@Component({
  selector: 'app-add-record',
  templateUrl: './add-record.component.html',
  styleUrls: ['./add-record.component.scss']
})
export class AddRecordComponent implements OnInit {
  title: any;
  btnTitle: any;
  token: any;
  action: any;
  docId:any;
  formData = new FormData();
  protuctForm = new FormGroup({
    product: new FormControl('', [Validators.required]),
    Institution: new FormControl('', [Validators.required]),
    branch: new FormControl('', [Validators.required]),
    expiry_date: new FormControl('', [Validators.required]),
    comments: new FormControl('', [Validators.required]),


  });
  myFiles: any;

  constructor(private activatedRoute: ActivatedRoute,private apiService:ApiServiceService,private router : Router,
    private store : StoreServiceService) {
    this.token = localStorage.getItem('key');
  }

  ngOnInit(): void {
    let id: any = this.activatedRoute.snapshot.paramMap.get("id");
    if (id == 1) {
      this.newSection();

    } else {
      this.editSection();
    }

  }
  newSection() {
    this.title = ' ADD NEW RECORD';
    this.btnTitle = 'ADD NEW RECORD';
    this.action = 'new'
  }
  editSection() {
    this.title = 'EDIT RECORD';
    this.btnTitle = 'UPDATE RECORD';
    this.action = 'update';
    this.store.data.subscribe(res=>{
      console.log("RR",res);
      this.docId = res.doc_id;
      this.protuctForm.patchValue({
        product: res.product_name,
        Institution: res.institution_name ,
        comments:res.comments ,
        expiry_date:new Date(res.expiry_date) ,
        branch:res.branch
      })
    })
  }
  record() {

    const formData = new FormData()
    let req = this.protuctForm.value;

    req['auth_token'] = this.token;
    req['user_id'] = 1;
    req['file'] = this.formData
    if (this.protuctForm.valid) {

      if (this.action === 'new') {
        console.log('a',req);
        
        this.apiService.getProduct(req).subscribe(res=>{
          if(res.success==true){
              this.router.navigate(['/home/dashboard'])
          }else{
            alert('try again')
          }
        })
        
      } else{
        console.log('b',req);
        req['doc_id'] = 1;
        this.apiService.editDocument(req).subscribe(res=>{
          if(res.success==true){
              this.router.navigate(['/home/dashboard'])
          }else{
            alert('try again')
          }
        })
      }

    } else {
      alert('Enter All Field')
    }

  }
  uploadFile(files: any) {
    this.myFiles = files.item(0);

    this.formData.append('file', this.myFiles, this.myFiles.name);
  }
}
