import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiServiceService } from 'src/app/service/api-service.service';
import { StoreServiceService } from 'src/app/service/store-service.service';
import {Md5} from 'ts-md5/dist/md5';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  token: any;
  userId: any;
  mockProductData = {

    "parameters": [
      {
        "pro_id": 1,
        "product_name": "Product 11",
        "is_active": 1
      },
      {
        "pro_id": 2,
        "product_name": "Product 2",
        "is_active": 1
      }
    ]
  }

  mockInsituctionData = {
    "success": true,
    "message": "Institution List",
    "parameters": [
      {
        "inst_id": 1,
        "institution_name": "Institution 11",
        "is_active": 1
      }, {
        "inst_id": 2,
        "institution_name": "Institution 22",
        "is_active": 1
      }
    ]
  }
  listOfDocument: any = [
    {
      "doc_id": 3,
      "product_id": 1,
      "institution_id": 1,
      "branch": "Branch",
      "expiry_date": "2021-12-01",
      "card_number": null,
      "comments": "comments",
      "is_active": 1,
      "added_by": 1,
      "added_at": "2020-12-04 09:56:13",
      "institution_name": "Institution 11",
      "product_name": "Product 11",
      "document_details": [
        {
          "id": 1,
          "document_id": 3,
          "doc_url": "test",
          "added_at": "2020-12-07 09:09:10"
        }
      ]
    },
    {
      "doc_id": 4,
      "product_id": 1,
      "institution_id": 1,
      "branch": "Branch",
      "expiry_date": "2021-12-01",
      "card_number": null,
      "comments": "HI",
      "is_active": 1,
      "added_by": 1,
      "added_at": "2020-12-04 09:56:13",
      "institution_name": "Institution 11",
      "product_name": "Product 11",
      "document_details": [
        {
          "id": 1,
          "document_id": 3,
          "doc_url": "test",
          "added_at": "2020-12-07 09:09:10"
        }
      ]
    }
  ]

  listData: any = []

  constructor(private router: Router, private apiService: ApiServiceService, private store: StoreServiceService) {
    this.token = localStorage.getItem('key');
   this.userId= localStorage.getItem('user_Id')
    this.mockProductData.parameters.map((res: any, i) => {
      if (res.pro_id == this.mockInsituctionData.parameters[i].inst_id) {
        this.listData.push({
          product_name: res.product_name,
          institution_name: this.mockInsituctionData.parameters[i].institution_name,
          pro_id: res.pro_id,
          inst_id: this.mockInsituctionData.parameters[i].inst_id
        })

      }
    })


  }

  ngOnInit(): void {
    this.getData()
  }
  async getData() {
    const md5 = new Md5();
    let salt = 'FutureWill@TT)(*&^!@2020';
    let auth_token = salt + this.userId;
    let  md5_token:any = md5.appendStr(auth_token).end();
    let docList = {
      "user_id": this.userId,
      "auth_token": this.token
    }

    await this.apiService.listDocument(docList).subscribe(list => {
      this.listOfDocument = [];
      this.listOfDocument = list.parameters;
    })
    let req = {
      auth_token: this.token
    }

    let products: any = [];
    let Insituctions: any = []
    await this.apiService.getProduct(req).subscribe(product => {
      products = product.parameters;
    });
    await this.apiService.getInsituction(req).subscribe(Insituction => {
      this.listData = [];
      Insituctions = Insituction.parameters;
      products.map((res: any, i: any) => {
        if (res.pro_id == Insituctions[i].inst_id) {
          this.listData.push({
            product_name: res.product_name,
            institution_name: Insituctions[i].institution_name,
            pro_id: res.pro_id,
            inst_id: Insituctions[i].inst_id
          })

        }
      })
    })

  }
  selectedRow(event: any) {
    console.log(event);
    this.store.updatedDataSelection(event)
    this.router.navigate(['/home/record/2'])
  }

  addNewRecord() {
    this.router.navigate(['/home/record/1'])
  }
}
